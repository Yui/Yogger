import { levelHandler } from "./types/logger.ts";

// deno-lint-ignore no-explicit-any
export const baseLevelHandler: Partial<levelHandler<any>> = {
  enabledLevels: true,
  enable(level) {
    if (this.enabledLevels == true) return this;
    this.enabledLevels!.add(level);
    return this;
  },
  disable(level) {
    if (this.enabledLevels == true) {
      this.enabledLevels = new Set();
      Object.values(this.levelEnum).forEach((val) => {
        // deno-lint-ignore no-explicit-any
        (this.enabledLevels as Set<any>).add(val);
      });
    }
    this.enabledLevels!.delete(level);
    return this;
  },
  enableAll() {
    this.enabledLevels = true;
    return this;
  },
  disableAll() {
    this.enabledLevels = new Set();
    return this;
  },
  _pass(level) {
    if (this.logLevel) {
      if ((level as number) < (this.logLevel as number)) return false;
    }

    if (this!.enabledLevels == true) return true;
    return this.enabledLevels!.has(level);
  },
  _convert(level) {
    if (typeof level === "number") {
      return Object.keys(this.levelEnum!).find((x) => this.levelEnum[x] == level);
    } else {
      return this.levelEnum[level];
    }
  },
};
