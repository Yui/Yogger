import type { enumType, transportErrorNotification } from "../types/logger.ts";
import type { fileTransportFormatter } from "../types/transports/file.ts";
import { defaultTransportErrorNotification } from "../utils.ts";
import { createStreamTransport } from "./stream.ts";

function init(filePath: string, reset: boolean): Deno.Writer & Deno.Closer {
  if (reset) {
    try {
      Deno.lstatSync(filePath);
      Deno.removeSync(filePath);
      // deno-lint-ignore no-empty
    } catch {}
  }
  const file = Deno.openSync(filePath, {
    create: true,
    append: true,
  });

  return file;
}

// deno-lint-ignore no-explicit-any
const defaultFileTransportFormatter: fileTransportFormatter<any> = (data) => {
  return [
    `[${data.time.toLocaleDateString()} ${data.time.toLocaleTimeString()}]`,
    data.level,
    Deno.inspect(data.message, { colors: false }),
    data.location?.filename ?? "",
    data.extra?.map((x) => Deno.inspect(x, { colors: false })).join(" ") ?? "",
  ];
};

export function createFileTransport<T extends enumType>(options: {
  levels: T;
  /** Re create the file on start. Default: false */
  reset?: boolean;
  formatter?: fileTransportFormatter<T>;
  /** Specify a path */
  filePath?: string;
  /** Specify only a filename. Relative path will be used. */
  fileName?: string;
  errorNotification?: transportErrorNotification;
}) {
  if (!options.fileName && !options.filePath) throw new Error("Specify either filename or filepath");

  return createStreamTransport({
    levels: options.levels,
    init: () => init(options.filePath ? options.filePath : `./${options.fileName}`, options.reset ?? false),
    errorNotification: options.errorNotification ?? defaultTransportErrorNotification,
    formatter: options.formatter ?? defaultFileTransportFormatter,
  });
}
