import { LokiLogger } from "https://deno.land/x/loki@0.1.1/mod.ts";
import { baseLevelHandler } from "../levelHandler.ts";
import type { enumType, transportErrorNotification } from "../types/logger.ts";
import type { lokiTransport, lokiTransportFormatter } from "../types/transports/loki.ts";
import { assign, defaultTransportErrorNotification } from "../utils.ts";
export type lokiTransportCore = LokiLogger;

// deno-lint-ignore no-explicit-any
const defaultLokiTransportFormatter: lokiTransportFormatter<any> = (data) => {
  return {
    text:
      Deno.inspect(data.message) +
      (data.extra && data.extra.length > 0 ? " " + data.extra.map((x) => Deno.inspect(x)).join(" ") : ""),
    meta: {
      level: data.level,
      file: data.location ? `${data.location.filename} L: ${data.location.line} C: ${data.location.col}` : "Unknown",
    },
  };
};

const defaultLokiTransportLabels: Record<string, string> = {
  job: "Yogger",
};

export function createLokiTransport<T extends enumType>(options: {
  levels: T;
  /** URL of your loki instance Example: 'http://localhost:3100' */
  url: string;
  /** How many logs will be sent in one chunk. */
  buffer: ConstructorParameters<typeof LokiLogger>[1];
  /** Gets called if error occures. By defaults logs to console. */
  errorNotification?: transportErrorNotification;
  formatter?: lokiTransportFormatter<T>;
  /** Loki labels which will get added to all logs. */
  labels?: Record<string, string>;
  // deno-lint-ignore no-explicit-any
}): lokiTransport<T, lokiTransport<T, any>> {
  const transport = Object.assign(
    {
      levelEnum: options.levels,
      core: new LokiLogger(options.url, options.buffer, options.labels ?? defaultLokiTransportLabels),
      errorNotification: options.errorNotification ?? defaultTransportErrorNotification,
      formatter: options.formatter ?? defaultLokiTransportFormatter,
    },
    baseLevelHandler
    // deno-lint-ignore no-explicit-any
  ) as unknown as lokiTransport<T, lokiTransport<T, any>>;

  // deno-lint-ignore no-explicit-any
  const handle: lokiTransport<T, any>["handle"] = async (data) => {
    const formatted = transport.formatter(data);

    try {
      await transport.core.log(data.level, formatted.text, formatted.meta);
    } catch (error) {
      return transport.errorNotification(error);
    }
  };

  assign(transport, {
    handle,
  });

  return transport;
}
