import { BufWriterSync } from "https://deno.land/std@0.159.0/io/buffer.ts";
import { baseLevelHandler } from "../levelHandler.ts";
import type { enumType, transportErrorNotification } from "../types/logger.ts";
import { advancedFileTransport, advancedFileTransportFormatter } from "../types/transports/advancedFile.ts";
import type { fileTransportFormatter } from "../types/transports/file.ts";
import { defaultTransportErrorNotification, fileExists } from "../utils.ts";

// deno-lint-ignore no-explicit-any
const defaultAdvancedFileTransportFormatter: advancedFileTransportFormatter<any> = (data) => {
  return [
    `[${data.time.toLocaleDateString()} ${data.time.toLocaleTimeString()}]`,
    data.level,
    Deno.inspect(data.message, { colors: false }),
    data.location?.filename ?? "",
    data.extra?.map((x) => Deno.inspect(x, { colors: false })).join(" ") ?? "",
  ];
};

export function createAdvancedFileTransport<T extends enumType>(options: {
  levels: T;
  /** Default: append */
  mode?: "overwrite" | "append" | "mustnotexist";
  formatter?: fileTransportFormatter<T>;
  /** Specify a path */
  filePath?: string;
  /** Specify only a filename. Relative path will be used. */
  fileName?: string;
  errorNotification?: transportErrorNotification;
  rotate?: {
    /** Maximum file size */
    maxBytes: number;
    /** How many files to keep excluding current one. */
    keepXFiles: number;
  };
}) {
  if (!options.fileName && !options.filePath) throw new Error("Specify either filename or filepath");

  if (!options.mode) options.mode = "append";

  const transport = Object.assign(
    {
      levelEnum: options.levels,
      formatter: options.formatter ?? defaultAdvancedFileTransportFormatter,
      errorNotification: options.errorNotification ?? defaultTransportErrorNotification,
      fileMode: {
        create: options.mode !== "mustnotexist",
        createNew: options.mode === "mustnotexist",
        append: options.mode === "append",
        truncate: options.mode === "overwrite",
        write: true,
      } as Deno.OpenOptions,
      filePath: options.filePath ? options.filePath : `./${options.fileName}`,
      rotate: options.rotate,
      setup: (() => {
        if (options.mode === "mustnotexist" && fileExists(transport.filePath, false))
          throw new Error("File must not exist");

        const file = Deno.openSync(transport.filePath, transport.fileMode);

        transport.stream = file;
        transport._buffer = new BufWriterSync(transport.stream);

        if (options.mode !== "append" && transport.rotate) {
          // Delete old files
          for (let i = 0; i <= transport.rotate?.keepXFiles; i++) {
            try {
              Deno.removeSync(`${transport.filePath}.${i}`);
            } catch (error) {
              if (!(error instanceof Deno.errors.NotFound)) throw error;
            }
          }
        }

        if (transport.rotate && options.mode === "append") {
          try {
            const size = Deno.lstatSync(transport.filePath).size;
            transport.rotate.fileSize = size;
          } catch {
            transport.rotate.fileSize = 0;
          }
        }

        // deno-lint-ignore no-explicit-any
      }) as advancedFileTransport<any, any>["setup"],
      destroy: (() => {
        transport._buffer.flush();
        transport.stream.close();
        // deno-lint-ignore no-explicit-any
      }) as advancedFileTransport<any, any>["destroy"],
    },
    baseLevelHandler
    // deno-lint-ignore no-explicit-any
  ) as advancedFileTransport<T, advancedFileTransport<T, any>>;

  if (transport.rotate) transport.rotate.fileSize = 0;

  // Overwrite handle
  transport.handle = (data) => {
    const formatted = transport.formatter(data);
    const byteArray =
      typeof formatted[0] === "string"
        ? new Uint8Array([...new TextEncoder().encode((formatted as string[]).join(" ")), 10])
        : (formatted as Uint8Array);

    if (transport.rotate) {
      const msgSize = byteArray.byteLength + 1;

      if (msgSize + transport.rotate.fileSize > transport.rotate.maxBytes) {
        // Rotate
        transport._buffer.flush();
        Deno.close(transport.stream.rid);

        for (let i = transport.rotate?.keepXFiles - 1; i >= 0; i--) {
          const og = `${transport.filePath}${i == 0 ? "" : `.${i}`}`;
          const newF = `${transport.filePath}.${i + 1}`;

          if (fileExists(og, false)) {
            Deno.renameSync(og, newF);
          }
        }

        transport.stream = Deno.openSync(transport.filePath, transport.fileMode);
        transport._buffer = new BufWriterSync(transport.stream);
        //
        transport.rotate.fileSize = 0;
      }

      transport._buffer.writeSync(byteArray);

      transport.rotate.fileSize += msgSize;
    } else transport._buffer.writeSync(byteArray);
  };

  return transport;
}
