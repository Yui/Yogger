import { baseLevelHandler } from "../levelHandler.ts";
import type { transport as transportType, transportErrorNotification } from "../types/logger.ts";
import { enumType } from "../types/logger.ts";
import type { streamTransport, streamTransportFormatter } from "../types/transports/stream.ts";
import { assign, defaultTransportErrorNotification } from "../utils.ts";

// deno-lint-ignore no-explicit-any
const defaultStreamTransportFormatter: streamTransportFormatter<any> = (data) => {
  return [
    `[${data.time.toLocaleDateString()} ${data.time.toLocaleTimeString()}]`,
    data.level,
    Deno.inspect(data.message, { colors: true }),
    data.location?.filename ?? "",
    data.extra?.map((x) => Deno.inspect(x, { colors: true })).join(" ") ?? "",
  ];
};

export function createStreamTransport<T extends enumType>(
  options: {
    levels: T;
    /** If you are returning string than new line will get appended automatically. */
    formatter?: streamTransportFormatter<T>;
    errorNotification?: transportErrorNotification;
  } & (
    | {
        /** Provide a stream here or via init method. */
        stream: Deno.Writer & Deno.Closer;
      }
    | {
        /** Return the stream to be used. */
        init: () => Deno.Writer & Deno.Closer;
      }
  )
  // deno-lint-ignore no-explicit-any
): streamTransport<T, streamTransport<T, any>> {
  // deno-lint-ignore no-explicit-any
  if (!(options as any).stream && !(options as any).init) {
    throw new Error("Specify either stream or init function");
  }

  const transport = Object.assign(
    {
      levelEnum: options.levels,
      formatter: options.formatter ?? defaultStreamTransportFormatter,
      stream: "stream" in options ? options.stream : undefined,
      setup:
        "init" in options
          ? () => {
              const stream = options.init();
              Reflect.set(transport, "stream", stream);
            }
          : undefined,
      errorNotification: options.errorNotification ?? defaultTransportErrorNotification,
    },
    baseLevelHandler
    // deno-lint-ignore no-explicit-any
  ) as unknown as streamTransport<T, streamTransport<T, any>>;

  // deno-lint-ignore no-explicit-any
  const handle: transportType<any>["handle"] = (data) => {
    const formatted = transport.formatter(data);
    const byteArray =
      typeof formatted[0] === "string"
        ? new Uint8Array([...new TextEncoder().encode((formatted as string[]).join(" ")), 10])
        : (formatted as Uint8Array);

    transport.stream.write(byteArray).catch(transport.errorNotification);
  };

  assign(transport, {
    handle,
  });

  return transport;
}
