import { baseLevelHandler } from "../levelHandler.ts";
import type { enumType, transport as transportType } from "../types/logger.ts";
import { assign } from "../utils.ts";

export function createBasicConsoleTransport<T extends enumType>(options: {
  levels: T;
  /** Default: true */
  colorExtra?: boolean;
}) {
  const transport = Object.assign(
    {
      levelEnum: options.levels,
    },
    baseLevelHandler
  ) as unknown as transportType<T>;

  // deno-lint-ignore no-explicit-any
  const handle: transportType<any>["handle"] = (data) => {
    console.log(
      `[${data.time.toLocaleDateString()} ${data.time.toLocaleTimeString()}]`,
      data.level,
      data.message,
      data.location?.filename,
      data.extra?.map((x) => Deno.inspect(x, { colors: options.colorExtra ?? true })).join(" ")
    );
  };

  assign(transport, {
    handle,
  });

  return transport;
}
