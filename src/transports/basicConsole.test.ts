import { assert } from "../deps.test.ts";
import { getLogLocation } from "../utils.ts";
import { createBasicConsoleTransport } from "./basicConsole.ts";

function locationTest() {
  return getLogLocation(-1);
}

const testObject = {
  message: "hello",
};

enum levels {
  info,
  warn,
  error,
  fatal,
}

const transport = createBasicConsoleTransport({ levels });

const baseLog = window.console.log;

Deno.test({
  name: "transports - basicConsole - logging",
  fn: async (t) => {
    await t.step({
      name: "test 1",
      fn: () => {
        window.console.log = (...data) => {
          assert(data.length == 5);
          assert(data[0] === "[3/15/1010 3:06:01 PM]");
          assert(data[1] === "info");
          assert(data[2] === "test");
          assert(data[3] == undefined);
          assert(data[4] == undefined);

          return baseLog(...data);
        };

        transport.handle({
          time: new Date(1010, 2, 15, 15, 6, 1, 20),
          message: "test",
          level: "info",
          levelIndex: 0,
        });
      },
    });

    await t.step({
      name: "test 2",
      fn: () => {
        window.console.log = (...data) => {
          assert(data.length == 5);
          assert(data[0] === "[3/15/1010 3:06:01 PM]");
          assert(data[1] === "info");
          assert(data[2] === "test");
          assert(data[3] == undefined);
          assert(Deno.inspect(data[4]) === String.raw`'{ message: \x1b[32m"hello"\x1b[39m }'`);

          return baseLog(...data);
        };

        transport.handle({
          time: new Date(1010, 2, 15, 15, 6, 1, 20),
          message: "test",
          level: "info",
          levelIndex: 0,
          extra: [testObject],
        });
      },
    });

    await t.step({
      name: "test 3",
      fn: () => {
        window.console.log = (...data) => {
          assert(data.length == 5);
          assert(data[0] === "[3/15/1010 3:06:01 PM]");
          assert(data[1] === "info");
          assert(data[2] === "test");
          assert(data[3] === "basicConsole.test.ts");
          assert(data[4] == undefined);

          return baseLog(...data);
        };

        transport.handle({
          time: new Date(1010, 2, 15, 15, 6, 1, 20),
          message: "test",
          level: "info",
          levelIndex: 0,
          location: locationTest(),
        });
      },
    });
  },
});
