import { baseLevelHandler } from "../levelHandler.ts";
import type { enumType, transport as transportType } from "../types/logger.ts";
import type { betterConsoleFormatter, betterConsoleTransport } from "../types/transports/betterConsole.ts";
import { assign } from "../utils.ts";

// deno-lint-ignore no-explicit-any
const defaultBetterConsoleTransportFormatter: betterConsoleFormatter<any> = (data) => {
  return [
    `[${data.time.toLocaleDateString()} ${data.time.toLocaleTimeString()}]`,
    data.level,
    Deno.inspect(data.message, { colors: true }),
    data.location?.filename ?? "",
    data.extra?.map((x) => Deno.inspect(x, { colors: true })).join(" "),
  ];
};

export function createBetterConsoleTransport<T extends enumType>(options: {
  levels: T;
  formatter?: betterConsoleFormatter<T>;
  // deno-lint-ignore no-explicit-any
}): betterConsoleTransport<T, betterConsoleTransport<T, any>> {
  const transport = Object.assign(
    {
      levelEnum: options.levels,
      formatter: options.formatter ?? defaultBetterConsoleTransportFormatter,
    },
    baseLevelHandler
    // deno-lint-ignore no-explicit-any
  ) as unknown as betterConsoleTransport<T, betterConsoleTransport<T, any>>;

  // deno-lint-ignore no-explicit-any
  const handle: transportType<any>["handle"] = (data) => {
    const formatted = transport.formatter(data);
    console.log(...formatted);
  };

  assign(transport, {
    handle,
  });

  return transport;
}
