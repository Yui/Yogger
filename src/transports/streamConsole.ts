import { enumType, transportErrorNotification } from "../types/logger.ts";
import type { streamConsoleFormatter, streamConsoleTransport } from "../types/transports/streamConsole.ts";
import { defaultTransportErrorNotification } from "../utils.ts";
import { createStreamTransport } from "./stream.ts";

// deno-lint-ignore no-explicit-any
const defaultStreamConsoleTransportFormatter: streamConsoleFormatter<any> = (data) => {
  return [
    `[${data.time.toLocaleDateString()} ${data.time.toLocaleTimeString()}]`,
    data.level,
    Deno.inspect(data.message, { colors: true }),
    data.location?.filename ?? "",
    data.extra?.map((x) => Deno.inspect(x, { colors: true })).join(" ") ?? "",
  ];
};

export function createStreamConsoleTransport<T extends enumType>(options: {
  levels: T;
  /** Default: out */
  std?: "out" | "err";
  formatter?: streamConsoleFormatter<T>;
  errorNotification?: transportErrorNotification;
}) {
  return createStreamTransport({
    levels: options.levels,
    stream: options.std === "err" ? Deno.stderr : Deno.stdout,
    formatter: options.formatter ?? defaultStreamConsoleTransportFormatter,
    errorNotification: options.errorNotification ?? defaultTransportErrorNotification,
    // deno-lint-ignore no-explicit-any
  }) as streamConsoleTransport<T, streamConsoleTransport<T, any>>;
}
