import { TokenBucket } from "https://x.nest.land/Yutils@2.1.2/modules/tokenbucket.ts";
import { baseLevelHandler } from "../levelHandler.ts";
import type { enumType } from "../types/logger.ts";
import type {
  discordWebhookTransport,
  discordWebhookTransportFormatter,
  errorNotification,
  limitNotification,
  webhookEmbed,
} from "../types/transports/discordWebhook.ts";
import type { AtLeastOne } from "../types/utils.ts";
import { assign, defaultTransportErrorNotification } from "../utils.ts";
export * from "https://deno.land/x/discordeno@13.0.0-rc20/types/embeds/mod.ts";
export { TokenBucketError } from "https://x.nest.land/Yutils@2.1.2/modules/tokenbucket.ts";

// deno-lint-ignore no-explicit-any
const defaultWebhookFormatter: discordWebhookTransportFormatter<any> = (data) => {
  const fields: webhookEmbed["fields"] = [
    {
      name: "message",
      value: Deno.inspect(data.message, { colors: false }),
    },
  ];

  if (data.location) {
    fields.push({
      name: "location",
      value: `${data.location.filename} L${data.location.line} C${data.location.col}`,
    });
  }

  return {
    embeds: [
      {
        title: data.level,
        type: "rich",
        fields: fields,
        timestamp: data.time.toISOString(),
      },
    ],
    file:
      data.extra && data.extra.length > 0
        ? {
            name: "extra.ts",
            blob: new Blob([data.extra!.map((x) => Deno.inspect(x)).join(" ")]),
          }
        : undefined,
  };
};

const defaultLimitNotification: limitNotification = () => {
  console.log("Logger - webhook transport limited.");
};

const webhookRegex = /^.*(discord|discordapp)\.com\/api\/webhooks\/([^\/]+)\/([^\/]+)/;

export function createDiscordWebhookTransport<T extends enumType>(options: {
  levels: T;
  webhook:
    | string
    | {
        id: bigint | string;
        token: string;
        threadId?: bigint | string;
        /** Custom username */
        username?: string;
        /** Custom avatar */
        avatar?: string;
      };
  formatter?: discordWebhookTransportFormatter<T>;
  rateLimits?: AtLeastOne<{
    /** Default: true
     * Logs which would go over the rate limit get dropped.
     */
    respectLimits: boolean;
    /** Gets called if webhook is limited. By default logs to console. */
    limitNotification: limitNotification;
  }>;
  /** Gets called if error occures. By defaults logs to console. */
  errorNotification?: errorNotification;
  /** Possibility to turn off webhook link validation */
  webhookLinkValidation?: boolean;
  // deno-lint-ignore no-explicit-any
}): discordWebhookTransport<T, discordWebhookTransport<T, any>> {
  if (
    options.webhookLinkValidation != false &&
    typeof options.webhook === "string" &&
    !webhookRegex.test(options.webhook)
  )
    throw new Error("Invalid webhook url.");

  const transport = Object.assign(
    {
      levelEnum: options.levels,
      formatter: options.formatter ?? defaultWebhookFormatter,
      webhookLink:
        typeof options.webhook === "string"
          ? options.webhook
          : `https://discord.com/api/webhooks/${options.webhook.id}/${options.webhook.token}${
              options.webhook.threadId
                ? `?${new URLSearchParams({ thread_id: options.webhook.threadId!.toString() })}`
                : ""
            }`,
      rateLimits: {
        respectLimits: options.rateLimits?.respectLimits ?? true,
        limitNotification: options.rateLimits?.limitNotification ?? defaultLimitNotification,
      },
      errorNotification: options.errorNotification ?? defaultTransportErrorNotification,
      customUsername: typeof options.webhook !== "string" ? options.webhook.username : undefined,
      customAvatar: typeof options.webhook !== "string" ? options.webhook.avatar : undefined,
    },
    baseLevelHandler
    // deno-lint-ignore no-explicit-any
  ) as unknown as discordWebhookTransport<T, discordWebhookTransport<T, any>>;

  // Limit 5/5s
  // TODO: Fine tune this
  const limiter = transport.rateLimits.respectLimits ? new TokenBucket(1, 1500, 1) : undefined;

  // deno-lint-ignore no-explicit-any
  const handle: discordWebhookTransport<T, any>["handle"] = async (data) => {
    if (limiter) {
      const ok = limiter.take();
      if (!ok) {
        transport.rateLimits.limitNotification();
        return;
      }
    }

    const formatted = transport.formatter(data);
    // If transport has avatar or username set and the formatter did not set one already than use the one from transport.
    if (!formatted.username) formatted.username = transport.customUsername;
    if (!formatted.avatar_url) formatted.avatar_url = transport.customAvatar;

    try {
      if (formatted.file) {
        const file = !Array.isArray(formatted.file) ? [formatted.file] : formatted.file;
        delete formatted.file;

        const form = new FormData();

        form.append("payload_json", JSON.stringify(formatted));

        file.forEach((f, i) => {
          form.append(`file${i}`, f.blob, f.name);
        });

        const res = await fetch(transport.webhookLink, {
          method: "POST",
          body: form,
        });

        if (!res.ok) {
          transport.errorNotification(new Error(res.statusText));
          return;
        }
      } else {
        const res = await fetch(transport.webhookLink, {
          headers: { "Content-Type": "application/json", Accept: "application/json" },
          method: "POST",
          body: JSON.stringify(formatted),
        });
        if (!res.ok) {
          transport.errorNotification(new Error(res.statusText));
          return;
        }
      }
    } catch (error) {
      transport.errorNotification(error);
      return;
    }
  };

  assign(transport, {
    handle,
  });

  return transport;
}
