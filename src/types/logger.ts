import type { genericFunction, ValueOf } from "./utils.ts";

// deno-lint-ignore no-explicit-any
export type enumType = any;

// deno-lint-ignore no-explicit-any
export interface levelHandler<T extends enumType, E extends any = any> {
  levelEnum: T;
  logLevel: ValueOf<T>;
  enabledLevels: Set<T> | true;
  enable: (level: ValueOf<T>) => E;
  disable: (level: ValueOf<T>) => E;
  enableAll: () => E;
  disableAll: () => E;
  /** Whether this should be logged based on the level. */
  _pass: (level: ValueOf<T>) => boolean;
  _convert: <S extends number | keyof T>(level: S) => S extends number ? keyof T : ValueOf<T>;
}

export interface logLocation {
  readonly path: string;
  readonly filename: string;
  readonly line: number;
  readonly col: number;
}

export type logPayload<T extends enumType, M extends "transport" | "base" = "base"> = {
  readonly level: M extends "base" ? ValueOf<T> : string;
  readonly time: Date;
  /** Main message */
  // deno-lint-ignore no-explicit-any
  readonly message: M extends "base" ? genericFunction | any : unknown;
  /** Extra data */
  readonly extra?: unknown[];
  readonly location?: logLocation;
} & (M extends "transport"
  ? {
      readonly levelIndex: number;
    }
  : // deno-lint-ignore ban-types
    {});

// deno-lint-ignore no-explicit-any
export interface transport<T extends enumType, E extends transport<T, any> = transport<T, any>>
  extends levelHandler<T, E> {
  /** Handles logs */
  handle: (data: logPayload<T, "transport">) => void;
  /** Called when logger is created. */
  setup?: () => void;
  /** Gets called when transport is deleted. */
  destroy?: () => void;
}

export type transportFormatter<T extends enumType, E> = (data: logPayload<T, "transport">) => E;
export type transportErrorNotification = (err: Error) => void;

export type loggerMethods<T extends enumType> = {
  // deno-lint-ignore no-explicit-any
  [key in keyof T]: <M extends any | genericFunction>(
    message: M,
    // deno-lint-ignore no-explicit-any
    ...extra: any[]
  ) => M extends genericFunction ? ReturnType<M> | void : M;
};

export type logFilter<T extends enumType> = (
  data: logPayload<T, "transport">,
  // deno-lint-ignore no-explicit-any
  transport: transport<T, transport<T, any>>
) => boolean;

export type logger<T extends enumType> = levelHandler<T, logger<T>> &
  loggerMethods<T> & {
    // deno-lint-ignore no-explicit-any
    transports: transport<T, any>[];
    filters?: logFilter<T>[];
    // deno-lint-ignore no-explicit-any
    _emit: (data: logPayload<T, "base">) => any;
    destroy: () => void;
  };
