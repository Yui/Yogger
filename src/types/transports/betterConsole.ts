import type { enumType, transport, transportFormatter } from "../logger.ts";

export type betterConsoleFormatter<T extends enumType> = transportFormatter<T, Parameters<typeof console["log"]>>;

export interface betterConsoleTransport<T extends enumType, E extends betterConsoleTransport<T, E>>
  extends transport<T, betterConsoleTransport<T, E>> {
  formatter: betterConsoleFormatter<T>;
}
