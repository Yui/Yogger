import type { lokiTransportCore } from "../../transports/loki.ts";
import type { enumType, transport, transportErrorNotification, transportFormatter } from "../logger.ts";

export type lokiTransportFormatter<T extends enumType> = transportFormatter<
  T,
  {
    text: string;
    meta?: Record<string, string>;
  }
>;

export interface lokiTransport<T extends enumType, E extends transport<T, E>>
  extends transport<T, lokiTransport<T, E>> {
  core: lokiTransportCore;
  formatter: lokiTransportFormatter<T>;
  errorNotification: transportErrorNotification;
}
