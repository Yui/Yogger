import type { enumType, transport, transportErrorNotification, transportFormatter } from "../logger.ts";

export type streamTransportFormatter<T extends enumType> = transportFormatter<T, string[] | Uint8Array>;

export interface streamTransport<T extends enumType, E extends streamTransport<T, E>>
  extends transport<T, streamTransport<T, E>> {
  formatter: streamTransportFormatter<T>;
  stream: Deno.Writer & Deno.Closer;
  errorNotification: transportErrorNotification;
}
