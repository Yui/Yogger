import type { enumType } from "../logger.ts";
import type { streamTransportFormatter } from "./stream.ts";

export type fileTransportFormatter<T extends enumType> = streamTransportFormatter<T>;
