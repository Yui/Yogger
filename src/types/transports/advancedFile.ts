import type { BufWriterSync } from "https://deno.land/std@0.159.0/io/buffer.ts";
import type { enumType, transport, transportErrorNotification } from "../logger.ts";
import type { streamTransportFormatter } from "./stream.ts";

export type advancedFileTransportFormatter<T extends enumType> = streamTransportFormatter<T>;

export interface advancedFileTransport<T extends enumType, E extends advancedFileTransport<T, E>>
  extends transport<T, advancedFileTransport<T, E>> {
  _buffer: BufWriterSync;
  stream: Deno.FsFile;
  formatter: advancedFileTransportFormatter<T>;
  errorNotification: transportErrorNotification;
  readonly fileMode: Deno.OpenOptions;
  readonly filePath: string;
  rotate?: {
    /** max file size */
    maxBytes: number;
    /** Amount of files to keep */
    keepXFiles: number;
    /** current file size */
    fileSize: number;
  };
}
