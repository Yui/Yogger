import type { enumType } from "../logger.ts";
import type { streamTransport, streamTransportFormatter } from "./stream.ts";

export type streamConsoleFormatter<T extends enumType> = streamTransportFormatter<T>;

export type streamConsoleTransport<T extends enumType, E extends streamConsoleTransport<T, E>> = streamTransport<T, E>;
