import type { Embed } from "https://deno.land/x/discordeno@13.0.0-rc20/types/embeds/mod.ts";
import type { enumType, transport, transportFormatter } from "../logger.ts";
import type { PossiblePromise } from "../utils.ts";

export type discordWebhookTransportFormatter<T extends enumType> = transportFormatter<T, webhookBody>;
export type limitNotification = () => PossiblePromise<void>;
export type errorNotification = (err: Error) => PossiblePromise<void>;

export interface discordWebhookTransport<T extends enumType, E extends transport<T, E>>
  extends transport<T, discordWebhookTransport<T, E>> {
  formatter: discordWebhookTransportFormatter<T>;
  webhookLink: string;
  rateLimits: {
    respectLimits: boolean;
    limitNotification: limitNotification;
  };
  errorNotification: errorNotification;
  customUsername?: string;
  customAvatar?: string;
}

export interface webhookFile {
  /** file blob */
  blob: Blob;
  /** name of file */
  name: string;
}

export interface webhookBody {
  content?: string;
  embeds?: webhookEmbed[];
  file?: webhookFile[] | webhookFile;
  /** Custom avatar */
  avatar_url?: string;
  /** Custom username */
  username?: string;
}

export interface webhookEmbed extends Omit<Embed, "type"> {
  /** Webhooks always use 'rich' */
  type: "rich";
}
