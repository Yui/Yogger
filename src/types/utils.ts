export type ValueOf<T> = T[keyof T];
export type AtLeastOne<T, U = { [K in keyof T]: Pick<T, K> }> = Partial<T> & U[keyof U];
export type PossiblePromise<T> = T | Promise<T>;
// deno-lint-ignore no-explicit-any
export type genericFunction = (...args: any) => any;
