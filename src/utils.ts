/** Allows easy way to add a prop to a base object when needing to use complicated getters solution. */

import { logLocation, transportErrorNotification } from "./types/logger.ts";

// deno-lint-ignore no-explicit-any
export function createNewProp(value: any): PropertyDescriptor {
  // Source: 2021 Discordeno - https://github.com/discordeno/discordeno/blob/37d63133dccd83f46a375192dbc779ce36a379db/src/util/utils.ts#L27
  return { configurable: true, enumerable: true, writable: true, value };
}

/** Parses log location via error stack */
export function getLogLocation(logIndex = 0): logLocation {
  logIndex += 2;
  const stackLines = new Error().stack!.split("\n");
  const importantLine = stackLines[logIndex + 1];

  const raw = importantLine?.trim();

  if (!raw?.includes(":"))
    return {
      path: raw,
      filename: "",
      line: 0,
      col: 0,
    };
  const regex = /(.+?)(?::(\d+))?(?::(\d+))?$/;
  const parts = regex.exec(raw.replace(/[()]/g, ""));

  const filePrefix = "file:///";
  parts![1] = parts![1].replaceAll("\\", "/"); // OS Compatibility
  const startIndex = parts![1].indexOf(filePrefix);
  const path = parts![1].slice(startIndex).replace(filePrefix, "");
  const filename = path.replace(/^.*[\\\/]/, "");

  return {
    path: path,
    filename: filename,
    line: parseInt(parts![2]),
    col: parseInt(parts![3]),
  };
}

// deno-lint-ignore ban-types
export function assign<T extends {}, U>(target: T, source: U): asserts target is T & U {
  Object.assign(target, source);
}

export const defaultTransportErrorNotification: transportErrorNotification = (err) => {
  console.error("Transport error", err);
};

export function fileExists<T extends boolean = true>(
  filePath: string,
  async: T
): T extends true ? Promise<boolean> : boolean {
  if (async != false) {
    return (async () => {
      try {
        await Deno.lstat(filePath);
        return true;
      } catch {
        return false;
      }
    })() as T extends true ? Promise<boolean> : boolean;
  } else {
    try {
      Deno.lstatSync(filePath);
    } catch {
      return false as T extends true ? Promise<boolean> : boolean;
    }
    return true as T extends true ? Promise<boolean> : boolean;
  }
}
