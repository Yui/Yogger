import { baseLevelHandler } from "./levelHandler.ts";
import { genericFunction } from "./mod.ts";
import type { enumType, logFilter, logger, logPayload, transport } from "./types/logger.ts";
import { getLogLocation } from "./utils.ts";

export async function createLogger<T extends enumType>(options: {
  transports: transport<T>[];
  filters?: logFilter<T>[];
  levels: T;
  /** Whether to parse where log was called from.
   * True - all log levels
   * False - do not parse at all
   * or you can specify which levels should have it
   */
  trackPaths?: boolean | (keyof T)[];
}): Promise<logger<T>> {
  const logger = Object.assign(
    {
      levelEnum: options.levels,
      transports: options.transports,
      filters: options.filters,
    },
    baseLevelHandler
  ) as unknown as logger<T>;

  // deno-lint-ignore no-explicit-any
  const emit: logger<any>["_emit"] = (data) => {
    const pass = logger._pass(data.level);

    // If function than return only if log will be created
    // If not function than we return value always
    /* This logic only follows logger logging options. 
    transport logging options are ignored as it would prob be too expensive to check. */

    if (pass) {
      const msg = data.message instanceof Function ? data.message() : data.message;

      // Convert level to string
      const levelString = logger._convert(data.level) as string;

      // Prepare transport data to avoid doing it for each transport
      const transportData: logPayload<T, "transport"> = {
        ...data,
        message: msg,
        level: levelString,
        levelIndex: data.level,
      };

      // Loop through transport
      for (const transport of logger.transports) {
        // Check transport log level
        if (transport._pass(data.level)) {
          // Check filters
          if (logger.filters) {
            let passedFilters = true;

            for (const filter of logger?.filters ?? []) {
              if (!filter(transportData, transport)) {
                passedFilters = false;
                break;
              }
            }

            // Return if filters did not pass
            if (!passedFilters) continue;
          }

          transport.handle(transportData);
        }
      }

      return msg;
    } else if (!(data.message instanceof Function)) return data.message;
  };

  Reflect.set(logger, "_emit", emit);

  for (const key in options.levels) {
    if (typeof key !== "string") continue;
    if (typeof options.levels[key] !== "number") continue;

    // deno-lint-ignore no-explicit-any
    Reflect.set(logger, key, (message: any | genericFunction, ...extra: unknown[]) => {
      return logger._emit({
        level: options.levels[key],
        time: new Date(),
        message: message,
        extra: extra,
        location:
          options.trackPaths == true || (Array.isArray(options.trackPaths) && options.trackPaths.includes(key))
            ? getLogLocation()
            : undefined,
      });
    });
  }

  for await (const transport of logger.transports) {
    // Skip disabled loggers
    if (!transport.setup || (typeof transport.enabledLevels === "object" && transport.enabledLevels.size == 0))
      continue;
    await transport.setup();
  }

  // Setup destroying to allow transports to safely finish
  Reflect.set(logger, "destroy", () => {
    for (const transport of logger.transports) {
      transport.destroy?.();
    }
  });

  // Destroy logger on exit
  addEventListener("unload", () => {
    logger.destroy();
  });

  return logger;
}
