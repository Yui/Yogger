# Yogger

- logger
- You can see examples [here](https://codeberg.org/Yui/Yogger/wiki#examples)

[![nest badge](https://nest.land/badge.svg)](https://nest.land/package/Yogger)

![Log Image 1](https://i.ibb.co/3Ygng2f/console.webp)

![Log Image 2](https://i.ibb.co/Jm3LshX/discordwebhook.webp)
